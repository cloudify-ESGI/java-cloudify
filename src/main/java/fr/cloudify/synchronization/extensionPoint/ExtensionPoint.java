package fr.cloudify.synchronization.extensionPoint;

public interface ExtensionPoint {
        void onLoginSuccessful();
        void onLoginFailed();
        void onSynchronizationSuccessful();
        void onSynchronizationFailed();
}
