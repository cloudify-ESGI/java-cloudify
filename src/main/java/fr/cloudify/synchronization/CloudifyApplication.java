package fr.cloudify.synchronization;

import fr.cloudify.synchronization.controller.AuthController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.logging.Level;
import java.util.logging.Logger;

@SpringBootApplication
public class CloudifyApplication extends Application {

	public static void main(String[] args) {
	    Logger.getGlobal().log(Level.SEVERE,"args : "+args[1]);
		SpringApplication.run(CloudifyApplication.class, args);
        try {
            launch(args);
        } catch(Throwable e) {
            Logger.getGlobal().log(Level.SEVERE, "Error while starting application.", e);
        }
        Logger.getGlobal().info("Application exit");
	}

    @Override
    public void start(Stage primaryStage){
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Login.fxml"));
            Pane rootLayout = (Pane) loader.load();
            Scene scene = new Scene(rootLayout);
            AuthController authController = (AuthController) loader.getController();
            authController.setStage(primaryStage);
            primaryStage.setTitle("Cloudify synchronization");
            scene.getStylesheets().add("style/style.css");
            primaryStage.setResizable(false);
            primaryStage.setScene(scene);
            primaryStage.show();

        } catch( Exception e ) {
            Logger.getGlobal().log(Level.SEVERE, "Error while loading the graphical interface.", e);
            Platform.exit();
        }
    }


}
