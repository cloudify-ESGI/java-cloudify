package fr.cloudify.synchronization.controller;

import fr.cloudify.synchronization.model.Configuration;
import fr.cloudify.synchronization.model.User;
import fr.cloudify.synchronization.networking.CloudifyRequest;
import fr.cloudify.synchronization.service.*;
import fr.cloudify.synchronization.service.PluginController;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.json.JSONObject;

import java.awt.*;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class AuthController {
	@FXML public Pane loginPane;
	public TextField login_email;
	public TextField login_password;
	private MainController mainController;
	private CloudifyRequest cloudifyRequest;
	private Stage stage;
	private UserController userController;
	private SynchronizationController synchronizationController;
	private PluginController pluginController;

	private static User user;
	private static String token;

	private static AuthController instance = null;

	public Configuration conf = null;
	public ConfigurationFileController confFileController = null;

	public void setStage(Stage stage){
        this.stage = stage;
    }

	public AuthController() {
    	//Instance controllers and cloudifyRequest class
		this.cloudifyRequest = new CloudifyRequest("/");
		this.mainController = new MainController();
		this.userController = new UserController();
		this.synchronizationController = SynchronizationController.getInstance();
		this.pluginController = PluginController.getInstance();

		this.conf = Configuration.getInstance();
		this.confFileController = ConfigurationFileController.getInstance();
		this.confFileController.loadConfFile();
		this.confFileController.saveState();

		instance = this;

	}

	public static AuthController getInstance(){
		return instance;
	}

	@FXML
	public void onLoginClicked() {
	    JSONObject returnValue;
		Map<String, String> header = new HashMap<>();
		JSONObject  params = new JSONObject();
		AuthController authController;

		params.put("email",login_email.getText());
		params.put("password",login_password.getText());

		try{
			returnValue = new JSONObject(this.cloudifyRequest.post("login",params.toString(),header)) ;
			if( returnValue.get("success").toString().equals("true") ){
				Logger.getGlobal().info(returnValue.toString());
				conf.setToken("JWT "+returnValue.get("message").toString());
				user = this.userController.convertJsonToUser((JSONObject) returnValue.get("user"));
				mainController.showMainPane(this.stage);
				onLoginSuccessful();
            }else{
				onLoginFailed();
			}
		}catch( Exception e){
			Logger.getGlobal().info(e.toString());
		}
	}


	@FXML
	public void onCloudifyLogoClicked() {
		Logger.getGlobal().info("IT WORKS");
	}

	@FXML
	public void onCreateAccountClicked() {

	}

	public static String getToken() {
		return token;
	}

	public static void setToken(String token) {
		AuthController.token = token;
	}

	public void onLoginSuccessful() {
		this.synchronizationController.launchSyncService();
		fr.cloudify.synchronization.service.PluginController.launchPluginsMethod("onLoginSuccessful");
	}

	public void onLoginFailed() {
		this.synchronizationController.launchSyncService();
		fr.cloudify.synchronization.service.PluginController.launchPluginsMethod("onLoginFailed");
	}

	public static User getUser() {
		return user;
	}
}
