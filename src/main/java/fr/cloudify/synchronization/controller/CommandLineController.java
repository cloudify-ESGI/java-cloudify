package fr.cloudify.synchronization.controller;

import fr.cloudify.synchronization.model.Configuration;
import fr.cloudify.synchronization.model.Directory;
import fr.cloudify.synchronization.model.Synchronization;
import fr.cloudify.synchronization.networking.CloudifyRequest;
import fr.cloudify.synchronization.service.ConfigurationFileController;
import fr.cloudify.synchronization.service.DirectoryController;
import fr.cloudify.synchronization.service.SynchronizationController;
import fr.cloudify.synchronization.service.UserController;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import org.apache.commons.cli.*;
import org.json.JSONObject;

import java.awt.*;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CommandLineController {

    private static CloudifyRequest cloudifyRequest;
    private static UserController userController;
    private static Configuration configuration;
    private static ConfigurationFileController configurationFileController;
    private static DirectoryController directoryController;
    private static SynchronizationController synchronizationController;

    public CommandLineController() {
        cloudifyRequest = new CloudifyRequest("/");
        directoryController = new DirectoryController();
        userController = new UserController();
        synchronizationController = SynchronizationController.getInstance();
        configuration = Configuration.getInstance();
        configurationFileController = ConfigurationFileController.getInstance();
        configurationFileController.loadConfFile();
    }

    public void launchCommandLine(String[] args) throws ParseException {
        final Options options = configParameters();

        final CommandLineParser parser = new DefaultParser();
        final CommandLine line = parser.parse(options, args);

        if( line.getOptionValue("login") != null && line.getOptionValue("password") != null){
            login(line.getOptionValue("login"),line.getOptionValue("password"));
        }

        if( line.hasOption("directory") ){
            displayDirectory(directoryController.get(0));
        }

        if( line.getOptionValue("create") != null && line.getOptionValue("localDir") != null ){
            if(line.getOptionValue("create").length() == 24){
                createSynchronization(line.getOptionValue("create"), line.getOptionValue("localDir"));
            }
        }

        if( line.hasOption("get") ){
            displaySynchronization(synchronizationController.get());
        }

        if( line.getOptionValue("remove") != null ){
            if(line.getOptionValue("remove").length() == 24){
                deleteSynchronization(line.getOptionValue("remove"));
            }
        }
    }

    private void deleteSynchronization(String remove) {
        synchronizationController.delete(remove);
    }

    private void displaySynchronization(List<Synchronization> synchronizationList) {
        for(Synchronization synchronization : synchronizationList){
            Logger.getGlobal().info(synchronization.getId()+" : "+synchronization.getLocalPath());
        }
    }

    private void createSynchronization(String dir, String localDir) {
        synchronizationController.post(synchronizationController.initSynchronization(dir,localDir));
    }

    private void displayDirectory(List<Directory> directories) {
        for(Directory directory : directories){
            Logger.getGlobal().info(directory.getName()+" : "+directory.getId());
        }
    }

    private void login(String login, String password){
        JSONObject returnValue;
        Map<String, String> header = new HashMap<>();
        JSONObject  params = new JSONObject();
        AuthController authController;

        params.put("email",login);
        params.put("password",password);

        Logger.getGlobal().info(configuration.toString());

        try{
            returnValue = new JSONObject(cloudifyRequest.post("login",params.toString(),header)) ;

            if( returnValue.get("success").toString().equals("true") ){

                configuration.setToken("JWT "+returnValue.get("message").toString());
                configuration.setEmail(userController.convertJsonToUser((JSONObject) returnValue.get("user")).getEmail());
                configurationFileController.saveState();
            }
        }catch( Exception e){
            Logger.getGlobal().info(e.toString());
        }
    }

    private static Options configParameters() {

        final Option loginOption = Option.builder("l")
                .longOpt("login")
                .desc("Identifiant")
                .hasArg(true)
                .argName("id")
                .required(false)
                .build();

        final Option passwordOption = Option.builder("p")
                .longOpt("password")
                .desc("Password")
                .hasArg(true)
                .argName("pwd")
                .required(false)
                .build();

        final Option directoryOption = Option.builder("d")
                .longOpt("directory")
                .desc("Directory")
                .hasArg(false)
                .argName("dir")
                .required(false)
                .build();

        final Option createSynchronizationOption = Option.builder("c")
                .longOpt("create")
                .desc("Create")
                .hasArg(true)
                .argName("c")
                .required(false)
                .build();

        final Option localDirOption = Option.builder("ld")
                .longOpt("localDir")
                .desc("localDir")
                .hasArg(true)
                .argName("ld")
                .required(false)
                .build();

        final Option getSynchronizationOption = Option.builder("g")
                .longOpt("get")
                .desc("get")
                .hasArg(false)
                .argName("get")
                .required(false)
                .build();

        final Option deleteSynchronizationOption = Option.builder("r")
                .longOpt("remove")
                .desc("Remove")
                .hasArg(true)
                .argName("r")
                .required(false)
                .build();

        final Options options = new Options();

        options.addOption(loginOption);
        options.addOption(passwordOption);
        options.addOption(directoryOption);
        options.addOption(createSynchronizationOption);
        options.addOption(localDirOption);
        options.addOption(getSynchronizationOption);
        options.addOption(deleteSynchronizationOption);

        return options;
    }
}
