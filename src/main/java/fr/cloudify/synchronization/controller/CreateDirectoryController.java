package fr.cloudify.synchronization.controller;

import fr.cloudify.synchronization.service.DirectoryController;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class CreateDirectoryController {

    public static Stage stage;

    public DirectoryController directoryController;
    public MainController mainController;
    public TextField directory_name_field;

    public CreateDirectoryController() {
        this.directoryController = new DirectoryController();
    }

    public void setMainController(MainController mainController){
        this.mainController = mainController;
    }

    public static void setStage(Stage primaryStage){
        stage = primaryStage;
    }

    public void onCreateDirectoryClicked(MouseEvent mouseEvent) {
        this.directoryController.post(this.directoryController.initDirectory(this.mainController.currentDirectory,directory_name_field.getText()));
        this.mainController.initializeView();
        stage.close();
    }
}
