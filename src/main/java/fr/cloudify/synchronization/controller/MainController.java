package fr.cloudify.synchronization.controller;

import fr.cloudify.synchronization.model.CloudifyFile;
import fr.cloudify.synchronization.model.Directory;
import fr.cloudify.synchronization.model.Synchronization;
import fr.cloudify.synchronization.service.DirectoryController;
import fr.cloudify.synchronization.service.FileController;
import fr.cloudify.synchronization.service.SynchronizationController;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainController{

    public Stage primaryStage;

    @FXML
    public Pane mainPane;

    @FXML
    public FlowPane folderContent;

    @FXML
    public TextFlow currentFolderName;

    @FXML
    public TextFlow parameterTab;

    @FXML
    public TextFlow folderTab;

    @FXML
    public Button synchronizeButton;

    @FXML
    public TextFlow currentDistanceFolderName;

    @FXML
    public FlowPane folderDetails;

    @FXML
    public Button deleteSyncButton;

    @FXML
    public Button addDirectoryButton;

    public Directory selectedDirectory;
    public Directory currentDirectory;
    public Synchronization currentSynchronization;

    public DirectoryController directoryController;
    public FileController fileController;
    public SynchronizationController synchronizationController;
    public TextFlow folderTabUser;
    public Button pluginButton;

    public void showMainPane(Stage primaryStage) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/main.fxml"));
            Pane rootLayout = (Pane) loader.load();
            Scene scene = new Scene(rootLayout);
            primaryStage.setTitle("Cloudify synchronization");
            primaryStage.setScene(scene);
            primaryStage.show();
            primaryStage.setOnCloseRequest(event -> {
                SynchronizationController synchronizationController = SynchronizationController.getInstance();
                synchronizationController.stopSyncService();
                System.exit(0);
            });
            this.primaryStage = primaryStage;
        } catch( Exception e ) {
            Logger.getGlobal().log(Level.SEVERE, "Error while loading the graphical interface.", e);
            Platform.exit();
        }

    }

    public MainController() {
        this.directoryController = new DirectoryController();
        this.fileController = new FileController();
        this.synchronizationController = SynchronizationController.getInstance();
        this.currentDirectory = null;
    }

    public void initialize(){
        initializeView();
    }

    public void initializeView(){
        cleanDisplay();
        addFolders(directoryController.get(0));
        addFiles(fileController.get(0));
        deleteSyncButton.managedProperty().bind(deleteSyncButton.visibleProperty());
        deleteSyncButton.setVisible(false);
    }

    public void cleanDisplay(){
        folderContent.getChildren().clear();
        folderDetails.getChildren().clear();
        currentDistanceFolderName.getChildren().clear();
    }

    private void addSynchronization(List<Synchronization> synchronizationList){
        folderDetails.getChildren().clear();
        if(synchronizationList.size()>0){
            Label label = new Label(synchronizationList.get(0).getLocalPath());
            folderDetails.getChildren().add(label);
            deleteSyncButton.setVisible(true);
            this.currentSynchronization = synchronizationList.get(0);
        }
    }

    private void addFolders(List<Directory> directoryList){
        folderContent.getChildren().clear();
        for(Directory directory : directoryList){
            addFolder(directory);
        }
    }

    private void addFolder(Directory directory){
        VBox folder = new VBox(10);
        folder.setMaxWidth(50);
        folder.setMaxHeight(80);
        folder.setAlignment(Pos.CENTER);
        Image image = new Image("view/folder.png",60,40,false,false);
        ImageView imageView = new ImageView(image);
        Label label1 = new Label(directory.getName());
        folder.getChildren().add(imageView);
        folder.getChildren().add(label1);
        folder.setOnMouseClicked(event -> {
            MouseButton button = event.getButton();
            if(button== MouseButton.PRIMARY){
                updateSelectedDirectory(directory);
            }

            if(button== MouseButton.SECONDARY){
                Logger.getGlobal().info("Test");
            }
        });

        folderContent.getChildren().add(folder);    

    }

    private void updateSelectedDirectory(Directory directory){
        currentDistanceFolderName.getChildren().clear();
        deleteSyncButton.setVisible(false);

        Text text = new Text(directory.getName());
        currentDistanceFolderName.getChildren().add(text);
        this.selectedDirectory = directory;
        addSynchronization(this.synchronizationController.get(this.selectedDirectory.getId()));
    }

    private void addFiles(List<CloudifyFile> cloudifyFileList){
        for(CloudifyFile cloudifyFile : cloudifyFileList){
            addFile(cloudifyFile);
        }
    }

    private void addFile(CloudifyFile cloudifyFile){
        VBox box = new VBox(10);
        box.setMaxWidth(50);
        box.setMaxHeight(80);
        box.setAlignment(Pos.CENTER);
        Image image = new Image("view/file.png",60,40,false,false);
        ImageView imageView = new ImageView(image);
        Label label1 = new Label(cloudifyFile.getName());
        Label label2 = new Label("Crée le :"+ cloudifyFile.getDateCreate());
        box.getChildren().add(imageView);
        box.getChildren().add(label1);
        box.getChildren().add(label2);

        folderContent.getChildren().add(box);

    }

    @FXML
    public void onSynchronizeClicked() {
        DirectoryChooser f = new DirectoryChooser();
        f.setTitle("Sélectionner un dossier à ajouter");
        File selectedFile;
        try {
            selectedFile =  f.showDialog(primaryStage);
            if( selectedFile != null && selectedFile.isDirectory() && selectedFile.list() == null){
                this.synchronizationController.post(this.synchronizationController.initSynchronization(this.selectedDirectory.getId(),selectedFile.getPath()));
                onSynchronizationSuccessful();
            }else{
                onSynchronizationFailed();
            }
        }catch( HeadlessException e){
            Logger.getGlobal().log(Level.SEVERE, "Error while opening folder browser.", e);
            onSynchronizationFailed();
        }
    }

    public void onSynchronizationSuccessful() {
        this.synchronizationController.launchSyncService();
        fr.cloudify.synchronization.service.PluginController.launchPluginsMethod("onSynchronizationSuccessful");
    }

    public void onSynchronizationFailed() {
        this.synchronizationController.launchSyncService();
        fr.cloudify.synchronization.service.PluginController.launchPluginsMethod("onSynchronizationFailed");
    }

    public void onCreateDirectoryClicked(MouseEvent mouseEvent) throws IOException {
        Stage subStage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/createDirectory.fxml"));
        Scene scene = new Scene(loader.load());
        CreateDirectoryController createDirectoryController = loader.getController();
        createDirectoryController.setStage(subStage);
        createDirectoryController.setMainController(this);
        subStage.setTitle("Nouveau dossier");
        subStage.setScene(scene);
        subStage.show();
    }

    public void onPluginClicked(MouseEvent mouseEvent) throws IOException {
        Stage subStage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/plugin.fxml"));
        Scene scene = new Scene(loader.load());
        PluginController pluginController = loader.getController();
        pluginController.setStage(subStage);
        pluginController.setMainController(this);
        subStage.setTitle("Plugin");
        subStage.setScene(scene);
        subStage.show();
    }

    public void onDeleteSynchronizationClicked(MouseEvent mouseEvent) {
        this.synchronizationController.delete(this.currentSynchronization.getId());
        initializeView();
    }


}
