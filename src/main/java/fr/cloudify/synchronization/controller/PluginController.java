package fr.cloudify.synchronization.controller;

import fr.cloudify.synchronization.model.Configuration;
import fr.cloudify.synchronization.model.Plugin;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.swing.filechooser.FileSystemView;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.lang.Thread.sleep;

public class PluginController {
    public static Stage stage;

    public TableColumn activated;
    @FXML
    public TableView pluginTable;
    public Button add_plugin;
    public MainController mainController;
    public ArrayList<Plugin> plugins;

    private Configuration configuration;

    public fr.cloudify.synchronization.service.PluginController pluginController;

    public PluginController() {

    }

    @FXML
    public void initialize(){
        plugins = new ArrayList<Plugin>();
        updateDisplayedPlugin();
    }

    public void updateDisplayedPlugin(){
        pluginTable.getItems().clear();
        this.configuration = Configuration.getInstance();

        this.pluginController = fr.cloudify.synchronization.service.PluginController.getInstance();

        for(File plugin : this.pluginController.pluginArray){
            plugins.add(new Plugin(plugin.getName()));
        }
        pluginTable.getItems().addAll(plugins);
    }

    public void setMainController(MainController mainController){
        this.mainController = mainController;
    }

    public static void setStage(Stage primaryStage){
        stage = primaryStage;
    }

    public void onAddPlugin(MouseEvent mouseEvent) {
        FileChooser f = new FileChooser();
        f.setTitle("Sélectionner un JAR à ajouter");
        f.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("JAR", "*.jar")
        );
        File selectedFile;
        try {
            selectedFile =  f.showOpenDialog(stage);
            if( selectedFile != null ){
                Files.copy(selectedFile.toPath(), Paths.get("Plugins/"+selectedFile.getName()));
                fr.cloudify.synchronization.service.PluginController.getInstance().openJarFiles();
                updateDisplayedPlugin();
            }
        }catch( HeadlessException | IOException e){
            Logger.getGlobal().log(Level.SEVERE, "Error while opening folder browser.", e);
        } catch (Exception e) {
            Logger.getGlobal().log(Level.SEVERE, "Can't load plugins.", e);
        }
    }


}
