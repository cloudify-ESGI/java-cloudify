package fr.cloudify.synchronization.model;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public final class Synchronization {
    private SimpleStringProperty id;
    private SimpleStringProperty directory;
    private SimpleStringProperty user;
    private SimpleStringProperty localPath;

    public Synchronization() {
        id = new SimpleStringProperty();
        directory = new SimpleStringProperty();
        user = new SimpleStringProperty();
        localPath = new SimpleStringProperty();
    }

    public String getId() {
        return id.get();
    }

    public SimpleStringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public String getDirectory() {
        return directory.get();
    }

    public SimpleStringProperty directoryProperty() {
        return directory;
    }

    public void setDirectory(String directory) {
        this.directory.set(directory);
    }

    public String getUser() {
        return user.get();
    }

    public SimpleStringProperty userProperty() {
        return user;
    }

    public void setUser(String user) {
        this.user.set(user);
    }

    public String getLocalPath() {
        return localPath.get();
    }

    public SimpleStringProperty localPathProperty() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath.set(localPath);
    }

    @Override
    public String toString() {
        return "Synchronization{" +
                "directory=" + directory +
                ", user=" + user +
                ", localPath=" + localPath +
                '}';
    }
}
