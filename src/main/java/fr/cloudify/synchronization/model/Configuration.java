package fr.cloudify.synchronization.model;

import fr.cloudify.synchronization.service.ConfigurationFileController;

import java.util.ArrayList;
import java.util.List;

public class Configuration {

    private static List<String> inUsePlugins = new ArrayList<>();
    private static String token;
    private static String email;

    private static Configuration instance = null;

    private Configuration() {
    }

    public static Configuration getInstance(){
        if(instance == null){
            instance = new Configuration();
        }
        return instance ;
    }


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getInUsePluginsString(){
        String returnValue = "";
        if(inUsePlugins.size() ==0)
            return "";
        if(inUsePlugins.size()== 1)
            return "{value : \""+inUsePlugins.get(0)+"\"}";
        return "{value : \""+String.join("\"},{value :\"",inUsePlugins)+"\"}";
    }

    public void addInUsePlugins(String name){
        this.inUsePlugins.add(name);
        ConfigurationFileController.getInstance().saveState();
    }

    public void removeInUsePlugins(String name){
        for( int i = 0 ; i < this.inUsePlugins.size(); i++ ){
            if(this.inUsePlugins.get(i).equals(name)){
                this.inUsePlugins.remove(i);
            }
        }
        ConfigurationFileController.getInstance().saveState();
    }

    public List<String> getInUsePlugins() {
        return inUsePlugins;
    }

    public String toString() {
        return "{\n"+
                "inUsePlugins : ["+this.getInUsePluginsString()+"],\n"+
                "email : \""+this.email+"\",\n"+
                "token : \""+this.token+"\"\n"+
                "}";
    }
}
