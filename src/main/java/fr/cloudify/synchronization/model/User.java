package fr.cloudify.synchronization.model;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import org.json.JSONObject;

public final class User {
    private SimpleStringProperty id;
    private SimpleStringProperty name;
    private SimpleStringProperty firstname;
    private SimpleStringProperty email;
    private SimpleStringProperty password;
    private SimpleStringProperty phone_number;
    private SimpleStringProperty address;
    private SimpleStringProperty rank;
    private SimpleStringProperty postal;
    private SimpleStringProperty city;

    public User (){
        id = new SimpleStringProperty();
        name = new SimpleStringProperty();
        firstname = new SimpleStringProperty();
        email = new SimpleStringProperty();
        password = new SimpleStringProperty();
        phone_number = new SimpleStringProperty();
        address = new SimpleStringProperty();
        rank = new SimpleStringProperty();
        postal = new SimpleStringProperty();
        city = new SimpleStringProperty();
        language = new SimpleStringProperty();
    }

    public String getId() {
        return id.get();
    }

    public SimpleStringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getFirstname() {
        return firstname.get();
    }

    public SimpleStringProperty firstnameProperty() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname.set(firstname);
    }

    public String getEmail() {
        return email.get();
    }

    public SimpleStringProperty emailProperty() {
        return email;
    }

    public void setEmail(String email) {
        this.email.set(email);
    }

    public String getPassword() {
        return password.get();
    }

    public SimpleStringProperty passwordProperty() {
        return password;
    }

    public void setPassword(String password) {
        this.password.set(password);
    }

    public String getPhoneNumber() {
        return phone_number.get();
    }

    public SimpleStringProperty phoneNumberProperty() {
        return phone_number;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phone_number.set(phoneNumber);
    }

    public String getAddress() {
        return address.get();
    }

    public SimpleStringProperty addressProperty() {
        return address;
    }

    public void setAddress(String address) {
        this.address.set(address);
    }

    public String getRank() {
        return rank.get();
    }

    public SimpleStringProperty rankProperty() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank.set(rank);
    }

    public String getPostal() {
        return postal.get();
    }

    public SimpleStringProperty postalProperty() {
        return postal;
    }

    public void setPostal(String postal) {
        this.postal.set(postal);
    }

    public String getCity() {
        return city.get();
    }

    public SimpleStringProperty cityProperty() {
        return city;
    }

    public void setCity(String city) {
        this.city.set(city);
    }

    public String getLanguage() {
        return language.get();
    }

    public SimpleStringProperty languageProperty() {
        return language;
    }

    public void setLanguage(String language) {
        this.language.set(language);
    }

    private SimpleStringProperty language;

    @Override
    public String toString() {
        return "User{" +
                "name=" + name +
                ", firstname=" + firstname +
                ", email=" + email +
                ", phoneNumber=" + phone_number +
                ", address=" + address +
                ", postal=" + postal +
                ", city=" + city +
                '}';
    }
}
