package fr.cloudify.synchronization.model;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public final class CloudifyFile {
    private SimpleStringProperty id;
    private SimpleStringProperty userCreate;
    private SimpleStringProperty userUpdate;
    private SimpleStringProperty directory;
    private SimpleStringProperty name;
    private SimpleStringProperty path;
    private SimpleStringProperty dateCreate;
    private SimpleIntegerProperty fileGroup;
    private SimpleStringProperty fileVersion;
    private SimpleStringProperty fileType;

    public CloudifyFile() {
        id = new SimpleStringProperty();
        userCreate = new SimpleStringProperty();
        userUpdate = new SimpleStringProperty();
        directory = new SimpleStringProperty();
        name = new SimpleStringProperty();
        path = new SimpleStringProperty();
        dateCreate = new SimpleStringProperty();
        fileGroup = new SimpleIntegerProperty();
        fileVersion = new SimpleStringProperty();
        fileType = new SimpleStringProperty();

    }

    public String getId() {
        return id.get();
    }

    public SimpleStringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public String getUserCreate() {
        return userCreate.get();
    }

    public SimpleStringProperty userCreateProperty() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate.set(userCreate);
    }

    public String getUserUpdate() {
        return userUpdate.get();
    }

    public SimpleStringProperty userUpdateProperty() {
        return userUpdate;
    }

    public void setUserUpdate(String userUpdate) {
        this.userUpdate.set(userUpdate);
    }

    public String getDirectory() {
        return directory.get();
    }

    public SimpleStringProperty directoryProperty() {
        return directory;
    }

    public void setDirectory(String directory) {
        this.directory.set(directory);
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getPath() {
        return path.get();
    }

    public SimpleStringProperty pathProperty() {
        return path;
    }

    public void setPath(String path) {
        this.path.set(path);
    }

    public String getDateCreate() {
        return dateCreate.get();
    }

    public SimpleStringProperty dateCreateProperty() {
        return dateCreate;
    }

    public void setDateCreate(String dateCreate) {
        this.dateCreate.set(dateCreate);
    }

    public int getFileGroup() {
        return fileGroup.get();
    }

    public SimpleIntegerProperty fileGroupProperty() {
        return fileGroup;
    }

    public void setFileGroup(int fileGroup) {
        this.fileGroup.set(fileGroup);
    }

    public String getFileVersion() {
        return fileVersion.get();
    }

    public SimpleStringProperty fileVersionProperty() {
        return fileVersion;
    }

    public void setFileVersion(String fileVersion) {
        this.fileVersion.set(fileVersion);
    }

    public String getFileType() {
        return fileType.get();
    }

    public SimpleStringProperty fileTypeProperty() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType.set(fileType);
    }

    @Override
    public String toString() {
        return "CloudifyFile{" +
                "id=" + id +
                ", userCreate=" + userCreate +
                ", userUpdate=" + userUpdate +
                ", directory=" + directory +
                ", name=" + name +
                ", path=" + path +
                ", dateCreate=" + dateCreate +
                ", fileGroup=" + fileGroup +
                ", fileVersion=" + fileVersion +
                ", fileType=" + fileType +
                '}';
    }
}
