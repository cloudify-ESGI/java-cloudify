package fr.cloudify.synchronization.model;

import fr.cloudify.synchronization.service.ConfigurationFileController;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.event.ActionEvent;

public class Plugin {

    public String name;

    @FXML
    public CheckBox activated = new CheckBox();

    public Plugin(String name) {
        this.name = name;
        if(Configuration.getInstance().getInUsePlugins().contains(name)){
            this.activated.setSelected(true);
        }
        this.activated.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(activated.isSelected() && !Configuration.getInstance().getInUsePlugins().contains(name)){
                    Configuration.getInstance().addInUsePlugins(name);
                }else if(!activated.isSelected() && Configuration.getInstance().getInUsePlugins().contains(name)){
                    Configuration.getInstance().removeInUsePlugins(name);
                }
            }
        });
    }

    public String getName() {
        return name;
    }

    public CheckBox getActivated() {
        return activated;
    }
}
