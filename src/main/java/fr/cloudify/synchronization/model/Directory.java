package fr.cloudify.synchronization.model;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public final class Directory {

    private SimpleStringProperty id;
    private SimpleStringProperty userCreate;
    private SimpleStringProperty userUpdate;
    private SimpleStringProperty parentDirectory;
    private SimpleStringProperty name;
    private SimpleStringProperty path;
    private SimpleStringProperty dateCreate;

    public Directory() {
        id = new SimpleStringProperty();
        userCreate = new SimpleStringProperty();
        userUpdate = new SimpleStringProperty();
        parentDirectory = new SimpleStringProperty();
        name = new SimpleStringProperty();
        path = new SimpleStringProperty();
        dateCreate = new SimpleStringProperty();
    }

    public String getId() {
        return id.get();
    }

    public SimpleStringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public String getUserCreate() {
        return userCreate.get();
    }

    public SimpleStringProperty userCreateProperty() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate.set(userCreate);
    }

    public String getUserUpdate() {
        return userUpdate.get();
    }

    public SimpleStringProperty userUpdateProperty() {
        return userUpdate;
    }

    public void setUserUpdate(String userUpdate) {
        this.userUpdate.set(userUpdate);
    }

    public String getParentDirectory() {
        return parentDirectory.get();
    }

    public SimpleStringProperty parentDirectoryProperty() {
        return parentDirectory;
    }

    public void setParentDirectory(String parentDirectory) {
        this.parentDirectory.set(parentDirectory);
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getPath() {
        return path.get();
    }

    public SimpleStringProperty pathProperty() {
        return path;
    }

    public void setPath(String path) {
        this.path.set(path);
    }

    public String getDateCreate() {
        return dateCreate.get();
    }

    public SimpleStringProperty dateProperty() {
        return dateCreate;
    }

    public void setDateCreate(String dateCreate) {
        this.dateCreate.set(dateCreate);
    }

    @Override
    public String toString() {
        return "Directory{" +
                "id=" + id +
                ", userCreate=" + userCreate +
                ", userUpdate=" + userUpdate +
                ", parentDirectory=" + parentDirectory +
                ", name=" + name +
                ", path=" + path +
                ", date=" + dateCreate +
                '}';
    }
}
