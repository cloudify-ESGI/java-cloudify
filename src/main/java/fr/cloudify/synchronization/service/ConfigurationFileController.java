package fr.cloudify.synchronization.service;

import fr.cloudify.synchronization.model.Configuration;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConfigurationFileController {
    private static Configuration configuration;

    private static ConfigurationFileController instance;

    private static File confFile;
    private static String path;

    private ConfigurationFileController() {
        this.configuration = Configuration.getInstance();
        confFile = new File("Plugins/.conf");
        path = confFile.getAbsolutePath();
        try {
            confFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadConfFile(){
        try {
            JSONObject newJson = new JSONObject(this.getConfString());
            this.convertJSONObjectToConf(newJson);
        } catch (Exception e) {
            Logger.getGlobal().log(Level.SEVERE,"Can't load configuration file");
        }
    }

    private String getConfString() throws Exception {
        File file = new File("Plugins/.conf");
        FileInputStream fis = new FileInputStream(file);
        byte[] data = new byte[(int) file.length()];
        fis.read(data);
        fis.close();

        String str = new String(data, "UTF-8");
        return str;
    }

    private void loadInUsePlugins(JSONObject json){
        JSONArray inUsePluginsJson = (JSONArray) json.get("inUsePlugins");
        for (int i = 0; i < inUsePluginsJson.length(); i++) {
            configuration.addInUsePlugins(inUsePluginsJson.getJSONObject(i).getString("value"));
        }
    }

    private void loadToken(JSONObject json){
        String token = (String) json.get("token");
        configuration.setToken(token);
    }

    private void loadEmail(JSONObject json){
        String email = (String) json.get("email");
        configuration.setEmail(email);
    }

    private void convertJSONObjectToConf(JSONObject json){
        this.loadInUsePlugins(json);
        this.loadToken(json);
        this.loadEmail(json);
    }

    public void saveState(){
        try {
            FileWriter fileWriter = new FileWriter(path);
            fileWriter.write(configuration.toString());
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ConfigurationFileController getInstance() {
        if(instance == null){
            instance = new ConfigurationFileController();
        }
        return instance;
    }
}
