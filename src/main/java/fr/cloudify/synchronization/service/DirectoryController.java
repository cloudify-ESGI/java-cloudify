package fr.cloudify.synchronization.service;

import fr.cloudify.synchronization.controller.AuthController;
import fr.cloudify.synchronization.model.Configuration;
import fr.cloudify.synchronization.model.Directory;
import fr.cloudify.synchronization.networking.CloudifyRequest;
import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Logger;

public class DirectoryController {

    private CloudifyRequest cloudifyRequest;
    public DirectoryController() {
        this.cloudifyRequest = new CloudifyRequest("/Directory/");
    }

    public List<Directory> get(int id){
        Logger.getGlobal().info("Begin Get Directory ");
        JSONArray returnValue = new JSONArray();
        Map<String, String> header = new HashMap<>();
        header.put("Authorization", Configuration.getInstance().getToken());

        try{
            returnValue = new JSONArray(this.cloudifyRequest.get("","",header));
        }catch( Exception e){
            Logger.getGlobal().info(e.toString());
        }
        Logger.getGlobal().info(returnValue.toString());
        Logger.getGlobal().info("End Get Directory ");
        return convertJsonArrayToDirectories(returnValue);
    }


    public Directory put(Directory directory){
        JSONObject body = new JSONObject();
        Map<String, String> header = new HashMap<>();
        header.put("Authorization",Configuration.getInstance().getToken());
        body = convertDirectoryToJson(directory);
        /*
        try{
            returnValue = new JSONObject(this.cloudifyRequest.put("",params.toString(),header));
        }catch( Exception e){
            Logger.getGlobal().info(e.toString());
        }*/
        if(body.length() != 0){
            return this.convertJsonToDirectory(body);
        }
        return null;
    }

    public boolean post(Directory directory){
        JSONObject body = convertDirectoryToJson(directory);
        Map<String, String> header = new HashMap<>();
        header.put("Authorization",Configuration.getInstance().getToken());

        try{
            this.cloudifyRequest.post("",body.toString(),header);
            return true;
        }catch( Exception e){
            Logger.getGlobal().info(e.toString());
        }
        return false;
    }

    public Directory convertJsonToDirectory(JSONObject json){
        Directory directory = new Directory();
        if(json.has("_id")){
            directory.setId(json.getString("_id"));
        }
        if(json.has("name")){
            directory.setName(json.getString("name"));
        }
        if(json.has("userUpdate")){
            directory.setUserUpdate(json.getString("userUpdate"));
        }
        if(json.has("parentDirectory")){
            directory.setParentDirectory(json.getString("parentDirectory"));
        }
        if(json.has("path")){
            directory.setPath(json.getString("path"));
        }
        if(json.has("dateCreate")){
            directory.setDateCreate(json.getString("dateCreate"));
        }
        return directory;
    }

    public JSONObject convertDirectoryToJson(Directory directory){
        if(directory == null)
            return null;
        JSONObject directoryJSON = new JSONObject();
        directoryJSON.put("name",directory.getName());
        directoryJSON.put("path",directory.getPath());
        directoryJSON.put("parentDirectory",directory.getParentDirectory());
        directoryJSON.put("date_create",directory.getDateCreate());
        return directoryJSON;
    }

    public List<Directory> convertJsonArrayToDirectories(JSONArray jsonArray) {
        List<Directory> directories = new ArrayList<>();
        for ( int i = 0 ; i < jsonArray.length() ; i++){
            directories.add(convertJsonToDirectory(jsonArray.getJSONObject(i)));
        }
        return directories;
    }

    public Directory initDirectory(Directory directory, String name){
        Directory newDirectory = new Directory();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        newDirectory.setDateCreate(dateFormat.format(date));
        newDirectory.setName(name);
        newDirectory.setPath("");
        if(directory != null){
            newDirectory.setParentDirectory(directory.getId());
        }
        return newDirectory;
    }
}
