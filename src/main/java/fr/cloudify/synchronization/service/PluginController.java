package fr.cloudify.synchronization.service;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PluginController {
    public ArrayList<String> plugins = new ArrayList<>();
    public File[] pluginArray;
    public ClassLoader loader;
    private static PluginController instance = null;
    public static File pluginPath;
    private static Map<Class,Object> map = new HashMap<Class,Object>();

    private PluginController() {
        pluginPath = this.getPluginPath();

        try {
            this.openJarFiles();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private File getPluginPath(){
        File theDir = new File("Plugins");

        if (!theDir.exists()) {
            Logger.getGlobal().info("creating directory: " + theDir.getName());
            try{
                theDir.mkdir();
            }catch(Exception se){
                Logger.getGlobal().log(Level.SEVERE, "Error while creating plugins folder");
            }
        }
        Logger.getGlobal().info("Created directory at: "+theDir.getAbsolutePath());
        return theDir;
    }

    public static PluginController getInstance(){
        if(instance == null){
            instance = new PluginController();
        }
        return instance;
    }

    public void findAllJar(File path){
        pluginArray = path.listFiles((dir, name) -> name.endsWith(".jar"));
    }

    public void convertJarToClass(File f) throws Exception {
        String fp = f.getAbsolutePath();
        loader = URLClassLoader.newInstance(new URL[] { f.toURL() }, getClass().getClassLoader());
        int i = 1;
        JarFile jar = new JarFile(fp);
        Enumeration<JarEntry> je = jar.entries();
        String convertedName;
        while (je.hasMoreElements()) {
            JarEntry entry = je.nextElement();
            if (entry.getName().endsWith(".class")) {
                convertedName = entry.getName().substring(0,entry.getName().length() - 6).replace('/','.'); // remove ".class" and format
                plugins.add(convertedName);

                Class<?> subClass = Class.forName(convertedName, true, loader);
                Constructor<?> subConst = subClass.getConstructor();
                Object doRun = subConst.newInstance();

                map.put(subClass,doRun);

                /*Method testaccess = subClass.getMethod("onLoginSuccessful");
                testaccess.invoke(doRun);*/

            }
        }
    }

    public static void launchPluginsMethod(String methodName) {
        Logger.getGlobal().info("________________________________________________________________");
        Logger.getGlobal().info(String.valueOf(map.entrySet().size()));
        for (Map.Entry<Class,Object> entry : map.entrySet()){
            Method testaccess = null;
            try{
                testaccess = entry.getKey().getMethod(methodName);
                if (testaccess != null) {
                    testaccess.invoke(entry.getValue());
                }
            }catch(Exception e){
                Logger.getGlobal().log(Level.SEVERE,"Can't call "+methodName+" of plugin");
            }

        }
    }

    public void openJarFile(File name) throws Exception {
        convertJarToClass(name);
    }

    public void openJarFiles() throws Exception {
        plugins.clear();
        findAllJar(pluginPath);
        for(File f: pluginArray){
            openJarFile(f);
        }
    }



}
