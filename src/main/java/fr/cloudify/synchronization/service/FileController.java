package fr.cloudify.synchronization.service;

import fr.cloudify.synchronization.controller.AuthController;
import fr.cloudify.synchronization.model.CloudifyFile;
import fr.cloudify.synchronization.model.Configuration;
import fr.cloudify.synchronization.networking.CloudifyRequest;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class FileController {

    private CloudifyRequest cloudifyRequest;
    public FileController() {
        this.cloudifyRequest = new CloudifyRequest("/file/");
    }


    public List<CloudifyFile> get(int id){
        Logger.getGlobal().info("Begin Get File ");
        JSONArray returnValue = new JSONArray();
        Map<String, String> header = new HashMap<>();
        header.put("Authorization", Configuration.getInstance().getToken());
        try{
            returnValue = new JSONArray(this.cloudifyRequest.get("","",header));
        }catch( Exception e){
            Logger.getGlobal().info(e.toString());
        }
            return convertJsonArrayToFiles(returnValue);
    }

    public CloudifyFile put(CloudifyFile cloudifyFile){
        JSONObject body;
        Map<String, String> header = new HashMap<>();
        JSONObject params = new JSONObject();
        header.put("Authorization",Configuration.getInstance().getToken());
        body = convertFileToJson(cloudifyFile);
        /*
        try{
            returnValue = new JSONObject(this.cloudifyRequest.put("",params.toString(),header));
        }catch( Exception e){
            Logger.getGlobal().info(e.toString());
        }*/
        if(body.length() != 0){
            return this.convertJsonToFile(body);
        }
        return null;
    }

    public CloudifyFile post(CloudifyFile cloudifyFile){
        JSONObject body = new JSONObject();
        Map<String, String> header = new HashMap<>();
        JSONObject params = new JSONObject();
        header.put("Authorization",Configuration.getInstance().getToken());
        body = convertFileToJson(cloudifyFile);
        /*
        try{
            returnValue = new JSONObject(this.cloudifyRequest.post("",params.toString(),header));
        }catch( Exception e){
            Logger.getGlobal().info(e.toString());
        }
        if(body.length() != 0){
            return this.convertFileToJson(body);
        }
        */
        return null;
    }

    public CloudifyFile convertJsonToFile(JSONObject json){
        CloudifyFile cloudifyFile = new CloudifyFile();
        if(json.has("user_create")){
            cloudifyFile.setUserCreate(json.getString("user_create"));
        }
        if(json.has("user_update")){
            cloudifyFile.setUserUpdate(json.getString("user_update"));
        }
        if(json.has("directory")){
            cloudifyFile.setDirectory(json.getString("directory"));
        }
        if(json.has("name")){
            cloudifyFile.setName(json.getString("name"));
        }
        if(json.has("path")){
            cloudifyFile.setPath(json.getString("path"));
        }
        if(json.has("date_create")){
            cloudifyFile.setDateCreate(json.getString("date_create"));
        }
        if(json.has("file_group")){
            cloudifyFile.setFileGroup(json.getInt("file_group"));
        }
        if(json.has("file_version")){
            cloudifyFile.setFileVersion(json.getString("file_version"));
        }
        if(json.has("file_type")){
            cloudifyFile.setFileType(json.getString("file_type"));
        }

        return cloudifyFile;
    }

    public JSONObject convertFileToJson(CloudifyFile cloudifyFile){
        JSONObject fileJSON = new JSONObject();
        fileJSON.put("user_create", cloudifyFile.getUserCreate());
        fileJSON.put("user_update", cloudifyFile.getUserUpdate());
        fileJSON.put("directory", cloudifyFile.getDirectory());
        fileJSON.put("name", cloudifyFile.getName());
        fileJSON.put("path", cloudifyFile.getPath());
        fileJSON.put("date_create", cloudifyFile.getDateCreate());
        fileJSON.put("file_group", cloudifyFile.getFileGroup());
        fileJSON.put("file_version", cloudifyFile.getFileType());
        fileJSON.put("file_type", cloudifyFile.getFileType());
        return fileJSON;
    }

    public List<CloudifyFile> convertJsonArrayToFiles(JSONArray jsonArray) {
        List<CloudifyFile> cloudifyFiles = new ArrayList<>();
        for ( int i = 0 ; i < jsonArray.length() ; i++){
            cloudifyFiles.add(convertJsonToFile(jsonArray.getJSONObject(i)));
        }
        return cloudifyFiles;
    }
}
