package fr.cloudify.synchronization.service;

import fr.cloudify.synchronization.controller.AuthController;
import io.socket.client.IO;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import static java.nio.file.LinkOption.NOFOLLOW_LINKS;
import static java.nio.file.StandardWatchEventKinds.*;

/**
 * Example to watch a directory (or tree) for changes to files.
 */

public class WatchDir {

    private final WatchService watcher;
    private final Map<WatchKey,Path> keys;
    private final boolean recursive;
    private AuthController authController = null;
    private SynchronizationController synchronizationController = null;

    @SuppressWarnings("unchecked")
    static <T> WatchEvent<T> cast(WatchEvent<?> event) {
        return (WatchEvent<T>)event;
    }

    private void register(Path dir) throws IOException {
        WatchKey key = dir.register(watcher, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
        keys.put(key, dir);
    }

    /**
     * Register the given directory, and all its sub-directories, with the
     * WatchService.
     */
    private void registerAll(final Path start) throws IOException {
        // register directory and sub-directories
        Files.walkFileTree(start, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
                    throws IOException
            {
                register(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }

    /**
     * Creates a WatchService and registers the given directory
     */
    public WatchDir(Path dir, boolean recursive) throws IOException {
        this.watcher = FileSystems.getDefault().newWatchService();
        this.keys = new HashMap<WatchKey,Path>();
        this.recursive = recursive;
        this.authController = AuthController.getInstance();
        this.synchronizationController = SynchronizationController.getInstance();

        if (recursive) {
            Logger.getGlobal().info("Scanning "+dir.toString()+" ...\n");
            registerAll(dir);
            Logger.getGlobal().info("Done.");
        } else {
            register(dir);
        }

    }

    /**
     * Process all events for keys queued to the watcher
     */
    public void processEvents(String syncId) throws URISyntaxException, Exception {
        io.socket.client.Socket socket = IO.socket("http://localhost:6789");

        socket.connect();

        while (true) {
            // wait for key to be signalled
            WatchKey key;
            try {
                key = watcher.take();
            } catch (InterruptedException x) {
                return;
            }

            Path dir = keys.get(key);
            if (dir == null) {
                System.err.println("WatchKey not recognized!!");
                continue;
            }

            for (WatchEvent<?> event: key.pollEvents()) {
                WatchEvent.Kind kind = event.kind();

                // TBD - provide example of how OVERFLOW event is handled
                if (kind == OVERFLOW) {
                    continue;
                }

                // Context for directory entry event is the file name of entry
                WatchEvent<Path> ev = cast(event);
                Path name = ev.context();
                Path child = dir.resolve(name);
                File fts = new File(child.toString());

                String parent = String.join("\\",Arrays.copyOf(child.toString().split("\\\\"),child.toString().split("\\\\").length-1));
                JSONObject parent2 = (JSONObject) SynchronizationController.getMapByKey(syncId).get(parent);

                System.out.format("%s: %s\n", event.kind().name(), child);
                Logger.getGlobal().info("map :"+SynchronizationController.getMapByKey(syncId).toString());
                Logger.getGlobal().info("child :"+child.toString());
                if(kind == ENTRY_CREATE) {
                    if(SynchronizationController.getMapByKey(syncId).has(child.toString())){
                        JSONObject current2 = (JSONObject) SynchronizationController.getMapByKey(syncId).get(child.toString());
                        Sender.sendIO(child.toString(), false, socket, parent2.getString("id"),AuthController.getUser().getId(),current2.get("id").toString());
                    }
                    Sender.sendIO(child.toString(), false, socket, parent2.getString("id"),AuthController.getUser().getId(),"0");

                } else if(kind == ENTRY_MODIFY) {
                    if(SynchronizationController.getMapByKey(syncId).has(child.toString())){
                        JSONObject current2 = (JSONObject) SynchronizationController.getMapByKey(syncId).get(child.toString());
                        Sender.sendIO(child.toString(), false, socket, parent2.getString("id"),AuthController.getUser().getId(),current2.get("id").toString());
                    }

                } else if(kind == ENTRY_DELETE) {
                    JSONObject current2 = (JSONObject) SynchronizationController.getMapByKey(syncId).get(child.toString());
                    Sender.sendIO(child.toString(), true, socket, "0","0",current2.get("id").toString());
                }
                this.synchronizationController.updateMapByKey(syncId);

                if (recursive && (kind == ENTRY_CREATE)) {
                    try {
                        if (Files.isDirectory(child, NOFOLLOW_LINKS)) {
                            registerAll(child);
                        }
                    } catch (IOException x) {
                        // ignore to keep sample readbale
                    }
                }
            }

            // reset key and remove from set if directory no longer accessible
            boolean valid = key.reset();
            if (!valid) {
                keys.remove(key);

                // all directories are inaccessible
                if (keys.isEmpty()) {
                    break;
                }
            }
        }
    }

}