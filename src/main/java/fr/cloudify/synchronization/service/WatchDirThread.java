package fr.cloudify.synchronization.service;

import org.json.JSONObject;

import java.nio.file.Path;
import java.util.logging.Logger;

public class WatchDirThread implements Runnable{
    public Path dir;
    public boolean isRecursive;
    public String syncId;

    public WatchDirThread(Path dir, boolean isRecursive,String syncId) {
        this.dir = dir;
        this.isRecursive = isRecursive;
        this.syncId = syncId;
    }

    @Override
    public void run() {
        try {
            Logger.getGlobal().info("In run of WatchDirThread");
            new WatchDir(this.dir, this.isRecursive).processEvents(syncId);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
