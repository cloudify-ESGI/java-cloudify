package fr.cloudify.synchronization.service;

import fr.cloudify.synchronization.controller.AuthController;
import fr.cloudify.synchronization.model.Configuration;
import fr.cloudify.synchronization.model.Directory;
import fr.cloudify.synchronization.model.Synchronization;
import fr.cloudify.synchronization.networking.CloudifyRequest;
import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SynchronizationController {

    private CloudifyRequest cloudifyRequest;

    public List<Thread> syncThread = new ArrayList<>();

    private static SynchronizationController instance;

    private static Map<String,JSONObject> map = new HashMap<String,JSONObject>();

    private SynchronizationController() {
        this.cloudifyRequest = new CloudifyRequest("/Synchronization/");
    }

    public static SynchronizationController getInstance(){
        if(instance == null){
            instance = new SynchronizationController();
        }
        return instance;
    }

    public List<Synchronization> get(String id){
        JSONArray returnValue;
        returnValue = new JSONArray();
        Map<String, String> header = new HashMap<>();
        header.put("Authorization", Configuration.getInstance().getToken());

        try{
            returnValue = new JSONArray(this.cloudifyRequest.get("directory/"+id,"",header));
        }catch( Exception e){
            Logger.getGlobal().info(e.toString());
        }
        return convertJsonArrayToSynchronizations(returnValue);
    }

    public List<Synchronization> get(){
        JSONArray returnValue;
        returnValue = new JSONArray();
        Map<String, String> header = new HashMap<>();
        header.put("Authorization", Configuration.getInstance().getToken());

        try{
            returnValue = new JSONArray(this.cloudifyRequest.get("user/","",header));
        }catch( Exception e){
            Logger.getGlobal().info(e.toString());
            Logger.getGlobal().info(e.toString());
            Logger.getGlobal().info(e.toString());
            Logger.getGlobal().info(e.toString());
            Logger.getGlobal().info(e.toString());
        }
        return convertJsonArrayToSynchronizations(returnValue);
    }

    public JSONObject loadMap(String syncId){
        JSONObject returnValue = new JSONObject();
        Map<String, String> header = new HashMap<>();
        header.put("Authorization", Configuration.getInstance().getToken());
        try{
            returnValue = new JSONObject(this.cloudifyRequest.get("map/"+syncId,"",header));
        }catch( Exception e){
            Logger.getGlobal().info(e.toString());
        }
        return returnValue;
    }

    public boolean delete(String synchronization){
        JSONObject returnValue = new JSONObject();
        Map<String, String> header = new HashMap<>();
        header.put("Authorization", Configuration.getInstance().getToken());
        try{
            this.cloudifyRequest.delete(synchronization,"",header);
            return true;
        }catch( Exception e){
            Logger.getGlobal().info(e.toString());
        }
        return false;
    }

    public Synchronization put(Synchronization synchronization){
        JSONObject body = new JSONObject();
        Map<String, String> header = new HashMap<>();
        JSONObject params = new JSONObject();
        header.put("Authorization", Configuration.getInstance().getToken());
        body = convertSynchronizationToJson(synchronization);

        if(body.length() != 0){
            return this.convertJsonToSynchronization(body);
        }
        return null;
    }

    public boolean post(Synchronization synchronization){
        JSONObject body = convertSynchronizationToJson(synchronization);
        Map<String, String> header = new HashMap<>();
        header.put("Authorization",Configuration.getInstance().getToken());

        try{
            this.cloudifyRequest.post("",body.toString(),header);
            return true;
        }catch( Exception e){
            Logger.getGlobal().info(e.toString());
        }
        return false;
    }

    public void launchSyncService(){
        List<Synchronization> allSync = this.get();
        if(allSync.size() == 0){
            Logger.getGlobal().log(Level.SEVERE,"No sync found");
            return;
        }
        for(Synchronization synchronization : allSync){
            Path dir = FileSystems.getDefault().getPath(synchronization.getLocalPath());
            map.put(synchronization.getId(),this.loadMap(synchronization.getId()));
            try{
                this.syncThread.add(new Thread(new WatchDirThread(dir,true,synchronization.getId())));
            }catch(Exception e){
                Logger.getGlobal().info(e.toString());
            }
        }
        if( this.syncThread.size() > 0){
            for(Thread thread : this.syncThread){
                thread.start();
            }
        }
        Logger.getGlobal().log(Level.SEVERE,"Here 1");
    }

    public void updateMapByKey(String syncId){
        map.put(syncId,this.loadMap(syncId));
    }

    public static JSONObject getMapByKey(String syncId) {
        return map.get(syncId);
    }

    public void stopSyncService(){
        if(this.syncThread.size() > 0){
            for(Thread thread : this.syncThread){
                thread.interrupt();
            }
        }
    }

    public Synchronization convertJsonToSynchronization(JSONObject json){
        Logger.getGlobal().info("Begin - In convertJsonToSynchronization");
        Synchronization synchronization = new Synchronization();
        if(json.has("_id")){
            synchronization.setId(json.getString("_id"));
        }
        if(json.has("directory")){
            synchronization.setDirectory(json.getString("directory"));
        }
        if(json.has("user")){
            synchronization.setUser(json.getString("user"));
        }
        if(json.has("local_path")){
            synchronization.setLocalPath(json.getString("local_path"));
        }
        Logger.getGlobal().info("End - In convertJsonToSynchronization");
        return synchronization;
    }

    public JSONObject convertSynchronizationToJson(Synchronization synchronization){
        JSONObject synchronizationJSON = new JSONObject();
        synchronizationJSON.put("directory",synchronization.getDirectory());
        synchronizationJSON.put("user",synchronization.getUser());
        synchronizationJSON.put("local_path",synchronization.getLocalPath());
        return synchronizationJSON;
    }

    public List<Synchronization> convertJsonArrayToSynchronizations(JSONArray jsonArray) {
        List<Synchronization> synchronizations = new ArrayList<>();
        for ( int i = 0 ; i < jsonArray.length() ; i++){
            synchronizations.add(convertJsonToSynchronization(jsonArray.getJSONObject(i)));
        }
        return synchronizations;
    }

    public Synchronization initSynchronization(String directory, String localPath){
        Synchronization newSynch = new Synchronization();
        newSynch.setLocalPath(localPath);
        newSynch.setDirectory(directory);
        return newSynch;
    }

}
