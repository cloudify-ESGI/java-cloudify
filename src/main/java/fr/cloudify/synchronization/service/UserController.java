package fr.cloudify.synchronization.service;

import fr.cloudify.synchronization.controller.AuthController;
import fr.cloudify.synchronization.model.Configuration;
import fr.cloudify.synchronization.model.User;
import fr.cloudify.synchronization.networking.CloudifyRequest;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class UserController {
    private CloudifyRequest cloudifyRequest;

    public UserController() {
        this.cloudifyRequest = new CloudifyRequest("/user/");
    }

    public User get(int id){
        JSONObject returnValue = new JSONObject();
        Map<String, String> header = new HashMap<>();
        JSONObject params = new JSONObject();
        header.put("Authorization", Configuration.getInstance().getToken());

        if(returnValue.length() != 0){
           return this.convertJsonToUser(returnValue);
        }
        return null;
    }

    public User convertJsonToUser(JSONObject json){
        User user = new User();
        user.setId(json.getString("_id"));
        user.setName(json.getString("name"));
        user.setFirstname(json.getString("firstname"));
        user.setEmail(json.getString("email"));
        user.setPhoneNumber(json.getString("phone_number"));
        user.setAddress(json.getString("address"));
        user.setPostal(json.getString("postal"));
        return user;
    }

}
