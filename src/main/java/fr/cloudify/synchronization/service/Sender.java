package fr.cloudify.synchronization.service;

import org.json.JSONObject;

import java.io.*;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.logging.Logger;

public class Sender {

    public static void sendIO(String fileToSend, boolean ToDelete, io.socket.client.Socket socket,String parent,String user,String id) throws Exception {

        File fts = new File(fileToSend);
        String folder = fileToSend.substring(14);
        Logger.getGlobal().info(id);
        Logger.getGlobal().info(fileToSend);
        Logger.getGlobal().info(fts.isDirectory() ? "true":"false");
        JSONObject obj = new JSONObject();
        obj.put("name", fts.getName());
        obj.put("id",id);
        if(fts.isDirectory()) {
            if(!ToDelete){
                obj.put("parent_directory",parent);
                obj.put("user",user);
                socket.emit("directory:create", obj);
            }else{
                socket.emit("directory:delete", obj);
            }

            return;
        }
        if(!ToDelete) {
            obj.put("parent_directory",parent);
            obj.put("user",user);
            obj.put("content", readFileAsString(fileToSend));
            obj.put("extension", getFileExtension(fts));
            socket.emit("file:create", obj);
        }
        else {
            socket.emit("file:delete", obj);
        }
    }

    public static String readFileAsString(String fileName)throws Exception {
        String data = "";
        data = new String(Files.readAllBytes(Paths.get(fileName)));
        return data;
    }

    private static String getFileExtension(File file) {
        String extension = "";
        try {
            if (file != null && file.exists()) {
                String name = file.getName();
                extension = name.substring(name.lastIndexOf("."));
            }
        } catch (Exception e) {
            extension = "";
        }
        return extension;
    }
}
