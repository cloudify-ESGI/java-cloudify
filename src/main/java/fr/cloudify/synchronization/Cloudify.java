package fr.cloudify.synchronization;

import java.util.logging.Level;
import java.util.logging.Logger;

import fr.cloudify.synchronization.controller.AuthController;
import fr.cloudify.synchronization.controller.CommandLineController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.apache.commons.cli.*;


public class Cloudify extends Application {
    @Override
    public void start(Stage primaryStage) {

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Login.fxml"));
            Pane rootLayout = (Pane) loader.load();
            Scene scene = new Scene(rootLayout);
            AuthController authController = (AuthController) loader.getController();
            authController.setStage(primaryStage);
            primaryStage.setTitle("Cloudify synchronization");
            scene.getStylesheets().add("style/style.css");
            primaryStage.setResizable(false);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch( Exception e ) {
            Logger.getGlobal().log(Level.SEVERE, "Error while loading the graphical interface.", e);
            Platform.exit();
        }

    }


    public static void main(String[] args) {
        if(args.length == 0){
            try {
                launch(args);
            } catch(Throwable e) {
                Logger.getGlobal().log(Level.SEVERE, "Error while starting application.", e);
            }
        }else{
            try {
                CommandLineController commandLineController = new CommandLineController();
                commandLineController.launchCommandLine(args);
            } catch (ParseException e) {
                Logger.getGlobal().log(Level.SEVERE, "Error while starting commandLine.", e);
            }
        }

        Logger.getGlobal().info("Application exit");
        System.exit(0);
    }





}
